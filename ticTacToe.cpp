/*

   Find keywords TODO's to fill in the missing codes

*/

#include <algorithm>
#include <random>
#include <set>
#include <queue>
#include <map>
#include <iterator>
#include <iostream>

using namespace std;

const char BLANK = '-' ; // BLANK square in the game board
const char X = 'X' ;
const char O = 'O' ;
const char TIE = BLANK;

// TODO: display all the steps playered at the game in the same row
void displayBoard ( const string& s, size_t i, bool printDigit ) {
  // TODO: fill in the missing codes
}

// TODO: display all the steps playered at the game in the same row
void displayStep ( const vector<string> & v, bool printDigit ) {
  // TODO: fill in the missing codes
  cout << "displayStep is not implemented yet!!!" << endl;
  cout << endl;
}

class Game {
public:
  friend ostream& operator<< (ostream& out, const Game& g ) {
    return out ;
  }
  Game () {
    m_board = string (9, BLANK);
  }
  void play () ;

private:
  void getInput ( char NP ) ;
  void playNextMove ( char P, char NP) ;
  void displayBoard ( bool printDigit = true ) const ;
  char findWinner ( ) const ;

  char nextMove ( char P, char NP) ; // helper function
  char evaluate ( char P ) ; // helper function
  inline bool boardFullyOccupied () const {
    return m_board.find (BLANK)==string::npos ;
  }

private:
  string m_board ;
  char   m_winningPlayer = TIE;
  vector<string> m_steps;;
};

/* 
  TODO: helper function:  check if play P will win or not
*/
bool isWinning( const string& s, const char& P ) {
  for (size_t i = 0; i < 3; ++i ) {
    bool w =  s[i*3] == P && s[i*3+1 ] == P && s[i*3+2 ] == P;
    w |=  s[i] == P && s[3+i] == P && s[6+i ] == P;
    if ( w ) return w;
  }
  bool w =  s[2] == P && s[4] == P && s[6] == P;
      w |=  s[0] == P && s[4] == P && s[8] == P;
  return w;
}

char Game::findWinner ( ) const {
  if ( isWinning(m_board, X) ) return X;
  if ( isWinning(m_board, O) ) return O;
  return TIE;
}

void Game::displayBoard ( bool printDigit ) const {
  cout << endl;
  string offset (10, ' ');
  for (size_t i = 0; i < 3; ++i ) {
    cout << offset ;
    for (size_t j = 0; j < 3; ++j ) {
      size_t k = i*3 + j;
      if ( m_board[k]==BLANK && printDigit  ) {
        cout << "  " << k + 1<< "   ";
      } else {
        cout << "  " << m_board[k] << "   ";
      }
    }
    cout << endl << endl;
  }
}

/*
  Evaluate, in a recursively manner,  the best move for current Player P.
  if P='O', P will maximize its winning opportunities.
  if P='X', P will minimize the winning opportunities of player 'O'.
*/

char Game::evaluate ( char currPlayer ) {
  char w = findWinner();
  // TODO: fill in the missing codes
  return  w;
}

void Game::getInput ( char NP) {
  displayBoard ( ) ;
  if ( m_winningPlayer != TIE ) {
    cout << "(Player " << m_winningPlayer << " is going to win!!!)" << endl;
  }
  int x ;
  for (;;) {
    cout << "Player " << NP << ", please input a number: " << endl;
    cin >> x;
    x--;
    if ( x>=0 && x<=8 && m_board[x]== BLANK) break;
  }
  m_board[x] = NP;
}

void Game::playNextMove ( char P, char NP) {
   m_winningPlayer = nextMove ( P, NP ) ;
}
char Game::nextMove ( char P, char NP) {

  if ( boardFullyOccupied ()  ) {
    return TIE ; 
  }

  char winner = NP; // assume winner is NP 
  size_t bestMove = 9 ; // 
  for (size_t i = 0 ; i < 9; ++i ) {
    if (m_board[i]!= BLANK ) continue;
    m_board[i] = P;
    char w = evaluate ( NP ) ;
    if ( w == P ) {
      winner = P; // done when find a decisive winning move 
      bestMove = i ;
      break; 
    } else if ( w == TIE ) { // tie
      winner = TIE; 
      bestMove = i ;
    } else if ( bestMove==9 ) {
      bestMove = i ;
    } 
    m_board[i] = BLANK; // reset the current move
  }   
  m_board[bestMove] = P;
  return winner;
}

bool peoplePlayFirst () {
  char ans = 'y' ;
  cout << "Do you want to play first? (y/n)" << endl;
  cin >> ans;
  return ans == 'y';
}
void displayResult ( char winner ) {
  if ( winner == TIE  ) {
    cout << "A tie game!!!" << endl;
  } else {
    cout << "Player " <<  winner << " won!!!" << endl;
  }
  cout << endl;
}

void Game::play () {
  char computer = O;
  char person = X ;
  char winner = BLANK;
  size_t i = 0 ;

  bool ans  = peoplePlayFirst () ;
  char goingToWin = BLANK;
  bool aTieGame = false;
  for (size_t i = 0 ; i < 9 && winner == TIE && !aTieGame; ++i ) {
    if ( (i%2==0) == ans ) {
      getInput ( person ) ;
    } else {
      playNextMove ( computer, person ) ;
    }
    winner = findWinner();
    m_steps.push_back ( m_board );
  }

  displayBoard ( false  ) ;
  displayResult ( winner ) ;
  displayStep ( m_steps, false ) ;
}

int main () {
  Game game ;
  game.play();
}
